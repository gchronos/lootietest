export enum AppHTMLRoutes {
  home = '/home',
}

export interface AppRoutingParams {
  id: string;
}

