import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';

import { HeaderComponent } from '@app/components/header/header.component';
import { MaterialModule } from '@app/core/modules/material.module';



const components = [
  HeaderComponent,
];

const sheets = [
];

const popups = [
];

const directives = [
];

const modules = [
  CommonModule,
  RouterModule,
  FormsModule,
  ReactiveFormsModule,
  HttpClientModule,
  MaterialModule,
];


@NgModule({
  imports: [
    ...modules
  ],
  declarations: [
    ...components,
    ...directives,
    ...sheets,
    ...popups
  ],
  exports: [
    ...modules,
    ...components,
    ...directives,
    ...sheets,
    ...popups
  ],
  providers: [
  ],
})
export class SharedModule {
}
