import {
  Component,
  OnInit
} from '@angular/core';
import { Router } from '@angular/router';

import { AppHTMLRoutes } from '../../app-routing.model';


@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  AppHTMLRoutes = AppHTMLRoutes;

  constructor(
    private router: Router,
  ) {
  }

  ngOnInit(): void {
  }
}



