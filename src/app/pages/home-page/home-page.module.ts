import { NgModule } from '@angular/core';

import { HomePageRoutingModule } from './home-page-routing.module';
import { SharedModule } from '../../core/modules/shared.module';
import { HomePageComponent } from './home-page.component';



@NgModule({
  imports: [
    SharedModule,
    HomePageRoutingModule,
  ],
  exports: [],
  declarations: [HomePageComponent],
})
export class HomePageModule {
}
